import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-create-qrcode',
  templateUrl: './create-qrcode.page.html',
  styleUrls: ['./create-qrcode.page.scss'],
})
export class CreateQrcodePage implements OnInit {

  qrData = null;
  createdCode = null;
  scannerCode = null;

  constructor(private barcodeScanner: BarcodeScanner) { }

  ngOnInit() {
  }

  create() {
    this.createdCode = this.qrData;
  }

  scan() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannerCode = barcodeData.text;
    });
  }



}
