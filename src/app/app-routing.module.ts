import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule)
  },

  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then(m => m.UserPageModule)
  },

  {
    path: 'lupa-sandi',
    loadChildren: () => import('./lupa-sandi/lupa-sandi.module').then(m => m.LupaSandiPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminPageModule)
  },


  {
    path: 'image-uploder',
    loadChildren: () => import('./image-uploder/image-uploder.module').then(m => m.ImageUploderPageModule)
  },


  {
    path: 'image-viewer',
    loadChildren: () => import('./media/image-viewer/image-viewer.module').then(m => m.ImageViewerPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profiles/profil/profil.module').then(m => m.ProfilPageModule)
  },
  {
    path: 'email',
    loadChildren: () => import('./profiles/email/email.module').then(m => m.EmailPageModule)
  },
  {
    path: 'password',
    loadChildren: () => import('./profiles/password/password.module').then(m => m.PasswordPageModule)
  },
  {
    path: 'create-qrcode',
    loadChildren: () => import('./create-qrcode/create-qrcode.module').then(m => m.CreateQrcodePageModule)
  },



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
