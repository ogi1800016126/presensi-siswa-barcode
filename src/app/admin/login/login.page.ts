import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import firebase from 'firebase/app';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: any = {};
  loading: boolean;

  constructor(
    public router: Router,
    public alert: AlertController,
    public auth: AngularFireAuth
  ) { }

  ngOnInit() {
  }



  async showAlert(header: string, message: string) {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ['ok']
    });
    await alert.present();
  }

  login() {
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res => {
      this.router.navigate(['/admin/user/home']);
    }).catch(err => {
      this.showAlert('Login Gagal', 'Pastikan Username dan Password anda benar');
      this.loading = false;
    });
  }

}
