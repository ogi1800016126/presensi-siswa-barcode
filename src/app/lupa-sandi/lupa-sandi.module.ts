import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LupaSandiPageRoutingModule } from './lupa-sandi-routing.module';

import { LupaSandiPage } from './lupa-sandi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LupaSandiPageRoutingModule
  ],
  declarations: [LupaSandiPage]
})
export class LupaSandiPageModule {}
