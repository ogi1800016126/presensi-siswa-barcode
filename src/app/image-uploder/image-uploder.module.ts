import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';

import { IonicModule } from '@ionic/angular';

import { ImageUploderPageRoutingModule } from './image-uploder-routing.module';

import { ImageUploderPage } from './image-uploder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageUploderPageRoutingModule
  ],
  declarations: [ImageUploderPage]
})
export class ImageUploderPageModule {}
