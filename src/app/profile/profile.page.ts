import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController, ModalController } from '@ionic/angular';
import { AlertService } from '../services/alert.service';
import { Router } from '@angular/router';
import { ImageUploderPage } from '../image-uploder/image-uploder.page';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from '../services/toast.service';
import { ProfilPage } from '../profiles/profil/profil.page';
import { EmailPage } from "../profiles/email/email.page";
import { PasswordPage } from "../profiles/password/password.page";

import { Plugins, CameraResultType } from '@capacitor/core';
const { Camera } = Plugins;


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    public auth: AngularFireAuth,
    public alert:AlertService,
    public alertController: AlertController,
    public router:Router,
    public modalController: ModalController,
    public db: AngularFirestore,
    public toast: ToastService
  ) {
    
  }

  userData:any={};
  ngOnInit()
  {
    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  } 

  user:any = {};
  getUser(email)
  {
    this.db.collection('users').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }
 
  //profile desc
  async updateProfile()
  {
    const modal = await this.modalController.create({
      component: ProfilePage,
      cssClass: 'my-custom-class',
      componentProps:{user: this.user, email: this.userData.email}
    });
    return await modal.present();
  }

  //profile desc
  async updateEmail()
  {
    const modal = await this.modalController.create({
      component: EmailPage,
      cssClass: 'my-custom-class',
      componentProps:{user: this.user, email: this.userData.email}
    });
    return await modal.present();
  }

  //profile desc
  async updatePassword()
  {
    const modal = await this.modalController.create({
      component: PasswordPage,
      cssClass: 'my-custom-class',
      componentProps:{user: this.user, email: this.userData.email}
    });
    return await modal.present();
  }

  //profile pic
  updatingProfileImage: boolean;
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });
    var imageUrl = image.dataUrl;
    var imagePath = this.userData.email+'/profile.png';
    const modal = await this.modalController.create({
      component: ImageUploderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: imagePath,ratio:1,width:100,height:100}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        this.updateUserData({profileUrl:{url: res.data.imageUrl,ref:imagePath}});
        this.updateProfileUrl(res.data.imageUrl);
      }
    });
    return await modal.present(); 
  }

  updateUserData(data)
  {
    this.updatingProfileImage = true;
    this.db.collection('users').doc(this.userData.email).update(data).then(res=>{
      this.updatingProfileImage = false;
    }).catch(err=>{
      this.updatingProfileImage = false;
      this.toast.present('Tidak dapat mengganti foto profil, coba lagi.','top');
    })
  }

  updateProfileUrl(url)
  {    
    this.auth.onAuthStateChanged(res=>{
      res.updateProfile({photoURL:url});
    });
  }


  

  async logout() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Keluar Aplikasi?',
      message: 'Untuk mengakses aplikasi ini, Anda perlu login kembali setelah keluar aplikasi.',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya, Keluar',
          handler: () => {
            this.auth.signOut();
            this.router.navigate(['/login']);
          }
        }
      ]
    });

    await alert.present();
  }
}